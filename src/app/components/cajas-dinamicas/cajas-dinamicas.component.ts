import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cajas-dinamicas',
  templateUrl: './cajas-dinamicas.component.html',
  styleUrls: ['./cajas-dinamicas.component.css']
})
export class CajasDinamicasComponent implements OnInit {

  
  form!: FormGroup;

  listaDeInputs: string[]= [];
  listaDeInputsCadena!: string;

  constructor(
    private fb: FormBuilder

  ) { 
    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  get cajasDeTexto() {
    return this.form.get('cajas') as FormArray;
  }



  crearFormulario(): void {

    this.form = this.fb.group({
      cajas: this.fb.array([])
    })

    // La caja por defecto
    this.cajasDeTexto.push(this.fb.group({
      nombre: [null, Validators.required]
    }))
  }

    // Para verificar si los carácteres son validos
    get nombreNoValido() {
      // si al obtener nombre este es invalido y es touched aplicaremos una clase de css en el html
      // el signo '?' de interrogación indica que sea opcional ya que al principio se muestra una casilla vacia ''
      return (
        this.form.get('nombre')?.invalid && this.form.get('nombre')?.touched
      );
    }


  agregarInput(): void {
    const nuevoUtil = this.fb.group({
      nombre: [null, Validators.required]
    })
    // agregamos un nuevo util al inicio de array
    this.cajasDeTexto.insert(0, nuevoUtil);
    
  }

  // limpia un input
  limpiarInput(id: number) {
    this.cajasDeTexto.at(id).get('nombre')?.setValue('');   
  }

  eliminarInput(id: number): void {
    this.cajasDeTexto.removeAt(id)
  }

  limpiarInputs(): void { 

    for (let i = 0; i < this.cajasDeTexto.length; i++) {
      // En cada posicion del array devolvemos su valor a ''
      this.cajasDeTexto.at(i).get('nombre')?.setValue(null);   


      // limpiamos a lista de cajas
      this.listaDeInputs = [];
      this.listaDeInputsCadena = this.listaDeInputsCadena.toString();
    }
    // reseteamos toda las cajas
    this.form.get('cajas')?.reset;
  }

  limpiarTextArea(): void {
    this.listaDeInputs = [];
    this.listaDeInputsCadena = this.listaDeInputs.toString();
  }


 guardarInputs(): void {

    this.listaDeInputs = [];

    for (let i = 0; i < this.cajasDeTexto.length; i++) { 
      
      if (this.cajasDeTexto.at(i).get('nombre')?.value != null)
      {
        this.listaDeInputs.push( this.cajasDeTexto.at(i).get('nombre')?.value );

        // Lo convertimos a string para que muestre en la caja con slato de linea
        this.listaDeInputsCadena = this.listaDeInputs.join('\n')
        // this.listaDeInputsCadena = this.listaDeInputsCadena.split("\n").join("<br>")
      }
      
    }

    console.log(this.listaDeInputs);
    
  }
}
